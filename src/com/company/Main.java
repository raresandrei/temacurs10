package com.company;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException {
        PersonRepositoryInterface personRepository =
                args[0].toLowerCase().equals("db") ? new PersonRepositoryDB() : new PersonRepository();

        UIInterface ui =
                args[1].toLowerCase().equals("gui")? new GUI(personRepository) : new UI(personRepository);

        ui.view();


    }
}
