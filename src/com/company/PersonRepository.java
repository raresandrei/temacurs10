package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class PersonRepository implements PersonRepositoryInterface{
    private static final File MAIN_DIR = new File("C:\\Users\\Rares Andrei\\IdeaProjects\\Curs10Interface\\src\\");

    public void create(Person person) {
        try {
            PrintWriter printWriter = new PrintWriter(new File(MAIN_DIR, person.getIdNumber()));
            printWriter.write(person.toString());
            printWriter.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
