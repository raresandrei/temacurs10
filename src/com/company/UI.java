package com.company;

import java.util.Scanner;

public class UI implements UIInterface {
    private PersonRepositoryInterface personRepository;

    public UI(PersonRepositoryInterface personRepository) {
        this.personRepository = personRepository;
    }

    public void view() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. C; 2. R; 3. U; 4. D; 5. E ");
            int operation = scanner.nextInt();

            switch (operation) {
                case 1:
                    create();
                    break;
                case 2:
                    read();
                    break;
                case 3:
                    update();
                    break;
                case 4:
                    delete();
                    break;
                case 5:
                    return;
                default:
                    throw new IllegalArgumentException("Invalid choice!");
            }
        }
    }

    private void create() {
        Scanner sc = new Scanner(System.in);
        System.out.println("ID: ");
        String idNum = sc.nextLine();
        System.out.println("Name: ");
        String name = sc.nextLine();
        System.out.println("Birth Date: ");
        String bd = sc.nextLine();

        personRepository.create(new Person(idNum, name, bd));
    }

    private void delete() {

    }

    private void update() {

    }

    private void read() {

    }
}
