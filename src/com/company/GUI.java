package com.company;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GUI extends JFrame implements UIInterface {
    private PersonRepositoryInterface personRepository;
    int intdex ;
    ArrayList<String> iter;
    JScrollPane scrollPane = new JScrollPane();
    public GUI(PersonRepositoryInterface personRepository) {

        this.setLayout(null);

        JLabel idL = new JLabel("ID: ");
        idL.setBounds(20, 20, 100, 20);
        this.add(idL);

        JLabel nameL = new JLabel("Name: ");
        nameL.setBounds(20, 40, 100, 20);
        this.add(nameL);

        JLabel bdL = new JLabel("Birth Date: ");
        bdL.setBounds(20, 60, 100, 20);
        this.add(bdL);

        JTextField idTf = new JTextField();
        idTf.setBounds(140, 20, 100, 20);
        this.add(idTf);

        JTextField nameTf = new JTextField();
        nameTf.setBounds(140, 40, 100, 20);
        this.add(nameTf);


        JTextField bdTf = new JTextField();
        bdTf.setBounds(140, 60, 100, 20);
        this.add(bdTf);

        JButton save = new JButton("Save");
        save.setBounds(70, 90, 100, 20);

//------
        JLabel idL2 = new JLabel("ID: ");
        idL2.setBounds(20, 280, 100, 20);
        this.add(idL2);

        JLabel nameL2 = new JLabel("Name: ");
        nameL2.setBounds(20, 300, 100, 20);
        this.add(nameL2);

        JLabel bdL2 = new JLabel("Birth Date: ");
        bdL2.setBounds(20, 320, 100, 20);
        this.add(bdL2);
        JTextField idE = new JTextField();
        idE.setBounds(140, 280, 100, 20);
        this.add(idE);

        JTextField nameE = new JTextField();
        nameE.setBounds(140, 300, 100, 20);
        this.add(nameE);


        JTextField bdE = new JTextField();
        bdE.setBounds(140, 320, 100, 20);
        this.add(bdE);

        JButton edit = new JButton("Edit and save");
        edit.setBounds(70, 350, 150, 20);

        iter= refresh();
        DefaultListModel<String> listModel = new DefaultListModel<>();

        JList<String> countryList;
        countryList = new JList<>(listModel);;
        countryList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        countryList.setBounds(140, 160, 300, 100);
        countryList.setVisibleRowCount(5);

        scrollPane.setBounds(140, 160, 300, 100);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setViewportView(countryList);
       // scrollPane.getViewport().addChangeListener(e -> JList.repaint());
        for(int i=1;i<iter.size();i+=3){
            listModel.addElement(iter.get(i));
        }

        save.addActionListener(e -> {personRepository.create(new Person(idTf.getText(), nameTf.getText(), bdTf.getText()));
            idTf.setText("");bdTf.setText("");nameTf.setText("");
            iter=refresh();
            scrollPane.removeAll();
            for(int i=1;i<iter.size();i+=3){
                listModel.addElement(iter.get(i));
            }
         //   revalidate();
           // repaint();
           // this.add(scrollPane);

        });
        this.add(save);

     //   for(int i=1;i<iter.size();i+=3){
     //       listModel.addElement(iter.get(i));
    //    }


        countryList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                if(!e.getValueIsAdjusting()) {
                  //  final List<String> selectedValuesList = countryList.getSelectedValuesList();
                  String selectedValue=countryList.getSelectedValue();
                    intdex=iter.indexOf(selectedValue);
                    System.out.println(intdex);
                    idE.setText(iter.get(intdex-1));
                    nameE.setText( iter.get(intdex ));
                    bdE.setText(iter.get(intdex+1));
                }
            }
        });
        this.add(scrollPane, BorderLayout.CENTER);



        edit.addActionListener(e -> {personRepository.create(new Person(idE.getText(), nameTf.getText(), bdTf.getText()));
            idE.setText("");bdE.setText("");nameE.setText("");
            File pacpac=new File("C:\\Users\\Rares Andrei\\IdeaProjects\\Curs10Interface\\src\\"+iter.get(intdex-1));
            if(pacpac.delete())
                System.out.println(pacpac.getName() + " was deleted!");


            refresh();
        });
        this.add(edit);


        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setBounds(50, 50, 500, 500);
    }

    public void view() {
        this.setVisible(true);
    }

    public ArrayList refresh(){
        ArrayList values= new ArrayList();
        try (DirectoryStream<Path> files = Files.newDirectoryStream(Paths.get("C:\\Users\\Rares Andrei\\IdeaProjects\\Curs10Interface\\src\\"),
                new DirectoryStream.Filter<Path>() {
                    @Override
                    public boolean accept(Path entry) throws IOException {
                        return !(new File(String.valueOf(entry)).isDirectory() );
                    }
                })
        ) {

            for (Path path : files) {
                System.out.println(path.toString());

                File file = new File(path.toString()); //new File("C:\\Users\\Rares Andrei\\IdeaProjects\\Curs10Interface\\src\\Rares");
                Scanner in =new Scanner(file);
                String line=in.nextLine();
                Pattern p = Pattern.compile("'([^' ]+)'");
                //Pattern p = Pattern.compile("\'([^\']*)\'");
                Matcher m = p.matcher(line);
                while (m.find()) {
                    System.out.println(m.group(1));
                    values.add(m.group(1));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return values;
    }
}
