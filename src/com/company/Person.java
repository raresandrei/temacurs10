package com.company;

public class Person {
    private String idNumber;
    private String name;
    private String birthDate;

    public Person(String idNumber, String name, String birthDate) {
        this.idNumber = idNumber;
        this.name = name;
        this.birthDate = birthDate;
    }

    public String getIdNumber() {
        return idNumber;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idNumber='" + idNumber + '\'' +
                ", name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }
}
