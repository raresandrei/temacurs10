package com.company;

public interface PersonRepositoryInterface {
    void create(Person person);
}
